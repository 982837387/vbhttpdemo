VERSION 5.00
Begin VB.Form HttpTest 
   Caption         =   "Http请求测试"
   ClientHeight    =   8475
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   ScaleHeight     =   8475
   ScaleWidth      =   8370
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton button2 
      Caption         =   "GET发送"
      Height          =   495
      Left            =   6960
      TabIndex        =   10
      Top             =   4200
      Width           =   1095
   End
   Begin VB.TextBox Text4 
      Height          =   495
      Left            =   1320
      TabIndex        =   8
      Text            =   "http://127.0.0.1:9090"
      Top             =   4200
      Width           =   5295
   End
   Begin VB.TextBox Text3 
      Height          =   495
      Left            =   1320
      TabIndex        =   6
      Text            =   "http://127.0.0.1"
      Top             =   3120
      Width           =   5295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "清空"
      Height          =   495
      Left            =   6960
      TabIndex        =   5
      Top             =   5280
      Width           =   1095
   End
   Begin VB.TextBox Text2 
      Height          =   2055
      Left            =   600
      TabIndex        =   4
      Top             =   600
      Width           =   6015
   End
   Begin VB.TextBox Text1 
      Height          =   2415
      Left            =   600
      TabIndex        =   3
      Top             =   5640
      Width           =   6135
   End
   Begin VB.CommandButton Button1 
      Caption         =   "POST发送"
      Height          =   495
      Left            =   6960
      TabIndex        =   0
      Top             =   3120
      Width           =   1095
   End
   Begin VB.Line Line2 
      X1              =   0
      X2              =   8520
      Y1              =   3960
      Y2              =   3960
   End
   Begin VB.Label Label2 
      Caption         =   "Get请求"
      Height          =   495
      Left            =   120
      TabIndex        =   9
      Top             =   4200
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Post请求"
      Height          =   495
      Left            =   120
      TabIndex        =   7
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Label Lable2 
      Caption         =   "Post请求："
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   1575
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   8880
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Label Lable2 
      Caption         =   "结果返回："
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   5280
      Width           =   1575
   End
End
Attribute VB_Name = "HttpTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'POST发送
Private Sub button1_Click()

    Dim str As String
    str = Text3.Text
    
    Dim JsonStr As String
    JsonStr = Text2.Text
    
    
    MsgBox "Input JSON string: " & JsonStr
    
    
   '发送http post请求
    Dim responseStr As String
    responseStr = HttpPOST(str, JsonStr)
   
   '文本框赋值
    Text1.Text = responseStr
 
End Sub

'GET发送
Private Sub button2_Click()
    Dim str As String
    str = Text4.Text
    
    Dim JsonStr As String
    JsonStr = Text2.Text
    
    Dim accessToken As String
    Dim api As Boolean
    accessToken = JSONParse("accessToken", JsonStr)
    api = JSONParse("api", JsonStr)

    '发送http post请求
    Dim responseStr As String
    responseStr = HttpGET(str, accessToken, api)

    '文本框赋值
    Text1.Text = responseStr
End Sub

'POST请求
'函数返回值是返回信息
'Url：发送的Url地址
'PostMsg：要发送的数据

Function HttpPOST(URL As String, PostMsg As String) As String
On Error GoTo er
Dim XMLHTTP As Object
Set XMLHTTP = CreateObject("Msxml2.XMLHTTP")
If Not IsObject(XMLHTTP) Then
    Set XMLHTTP = CreateObject("Microsoft.XMLHTTP")
    If Not IsObject(XMLHTTP) Then Exit Function
End If

'' 同步接收数据
XMLHTTP.Open "POST", URL, False
XMLHTTP.SetRequestHeader "CONTENT-TYPE", "application/json"
XMLHTTP.Send (PostMsg)

Do While XMLHTTP.ReadyState <> 4
    DoEvents
Loop

If XMLHTTP.Status = 200 Then
    HttpPOST = XMLHTTP.ResponseText
Else
    HttpPOST = ""
End If

Exit Function
er:
    MsgBox "发送POST请求失败！", , "提示"
End Function


'GET请求

Function HttpGET(URL As String, accessToken As String, api As Boolean) As String
On Error GoTo er
Dim XMLHTTP As Object
Set XMLHTTP = CreateObject("Msxml2.XMLHTTP")
If Not IsObject(XMLHTTP) Then
    Set XMLHTTP = CreateObject("Microsoft.XMLHTTP")
    If Not IsObject(XMLHTTP) Then Exit Function
End If

'' 同步接收数据
XMLHTTP.Open "GET", URL, False
XMLHTTP.SetRequestHeader "accessToken", accessToken
XMLHTTP.SetRequestHeader "api", api
XMLHTTP.SetRequestHeader "CONTENT-TYPE", "application/json"
XMLHTTP.Send

Do While XMLHTTP.ReadyState <> 4
    DoEvents
Loop

If XMLHTTP.Status = 200 Then
    HttpGET = XMLHTTP.ResponseText
Else
    HttpGET = ""
End If

Exit Function
er:
    MsgBox "发送GET请求失败！", , "提示"
End Function


'方法一
Public Function POST(ByVal URL As String, ByVal JSONData As String) As String
    Dim HTTP As Object
    Set HTTP = CreateObject("WinHttp.WinHttpRequest.5.1")
    HTTP.Option(6) = False
    HTTP.Option(4) = 13056
    HTTP.Open "POST", URL
    HTTP.SetRequestHeader "Content-Type", "application/json"
    HTTP.SetRequestHeader "Content-Length", LenB(StrConv(JSONData, vbFromUnicode))
    HTTP.Send JSONData
    
    POST = HTTP.ResponseText
    Set HTTP = Nothing
End Function



Public Function JSONParse(ByVal JSONPath As String, ByVal JSONString As String) As Variant
    Dim JSON As Object
    Set JSON = CreateObject("MSScriptControl.ScriptControl")
    JSON.language = "JScript"
    JSONParse = JSON.Eval("JSON=" & JSONString & ";JSON." & JSONPath & ";")
    Set JSON = Nothing
End Function


Private Sub Command1_Click()
'文本框赋值
    Text1.Text = ""
End Sub
